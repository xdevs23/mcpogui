# McPogUI

aka PogUI aka MCWM aka very pog UI framework with API

## License

This software is licensed under the GPL v3.0.

```
McPogUI UI Framework
Copyright (C) 2020 Simão Gomes Viana. All Rights Reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, only version 3 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
```

All source files in this project (this directory and all subdirectories) are subject
to the license outlined above. Source files include, but are not limited to, any file
that has the extension `.java`, `.kt`, `.gradle`, `.groovy` or `.sh`.

Source files do **not** include files with the extension `.jar`.
