package wtf.uuh.mc.pogui.geometry

enum class HorizontalAlignment {
    LEFT, CENTER, RIGHT
}