package wtf.uuh.mc.pogui.geometry

enum class VerticalAlignment {
    TOP, CENTER, BOTTOM
}