package wtf.uuh.mc.pogui.geometry

import kotlin.math.roundToInt

data class Point(var x: Double = .0, var y: Double = .0) {

    constructor(x: Int, y: Int) : this(x.toDouble(), y.toDouble())

    operator fun unaryPlus() = Point(+x, +y)
    operator fun unaryMinus() = Point(-x, -y)
    operator fun plus(other: Point) = Point(x + other.x, y + other.y)
    operator fun minus(other: Point) = Point(x - other.x, y - other.y)
    operator fun times(other: Point) = Point(x * other.x, y * other.y)
    operator fun div(other: Point) = Point(x / other.x, y / other.y)

    operator fun plusAssign(other: Point) {
        x += other.x
        y += other.y
    }

    operator fun minusAssign(other: Point) {
        x -= other.x
        y -= other.y
    }

    operator fun timesAssign(other: Point) {
        x *= other.x
        y *= other.y
    }

    operator fun divAssign(other: Point) {
        x /= other.x
        y /= other.y
    }

    operator fun inc() = Point(x + 1, y + 1)
    operator fun dec() = Point(x - 1, y - 1)

    fun reset(): Point {
        x = .0
        y = .0
        return this
    }

    val xI: Int
        get() = x.roundToInt()

    val yI: Int
        get() = y.roundToInt()

    fun subtract(point: Point): Point {
        x -= point.x
        y -= point.y
        return this
    }

    fun multiply(point: Point): Point {
        x *= point.x
        y *= point.y
        return this
    }

    fun divide(point: Point): Point {
        x /= point.x
        y /= point.y
        return this
    }

    fun clamp(
        minX: Double,
        minY: Double,
        maxX: Double,
        maxY: Double
    ): Point {
        x = x.coerceIn(minX, maxX)
        y = y.coerceIn(minY, maxY)
        return this
    }

    override fun hashCode(): Int {
        var result = x.hashCode()
        result = 31 * result + y.hashCode()
        return result
    }

    override operator fun equals(other: Any?): Boolean {
        if (other !is Point) {
            return false
        }
        return x == other.x && y == other.y || super.equals(other)
    }

    override fun toString(): String {
        return "{$x,$y}"
    }

    companion object {
        val zero get() = Point(0, 0)
    }

}