package wtf.uuh.mc.pogui.geometry

import kotlin.math.roundToInt

data class Rectangle(var width: Double, var height: Double) {

    constructor(width: Int, height: Int) : this(width.toDouble(), height.toDouble())

    fun copy(): Rectangle {
        return Rectangle(width, height)
    }

    operator fun unaryPlus() = Rectangle(+width, +height)
    operator fun unaryMinus() = Rectangle(-width, -height)

    operator fun plus(other: Rectangle) = Rectangle(width + other.width, height + other.height)
    operator fun minus(other: Rectangle) = Rectangle(width - other.width, height - other.height)
    operator fun times(other: Rectangle) = Rectangle(width * other.width, height * other.height)
    operator fun div(other: Rectangle) = Rectangle(width / other.width, height / other.height)

    operator fun plusAssign(other: Rectangle) {
        width += other.width
        height += other.height
    }

    operator fun minusAssign(other: Rectangle) {
        width -= other.width
        height -= other.height
    }

    operator fun timesAssign(other: Rectangle) {
        width *= other.width
        height *= other.height
    }

    operator fun divAssign(other: Rectangle) {
        width /= other.width
        height /= other.height
    }

    operator fun inc() = Rectangle(width + 1, height + 1)
    operator fun dec() = Rectangle(width - 1, height - 1)

    fun reset(): Rectangle {
        width = 0.0
        height = 0.0
        return this
    }

    fun center(width: Boolean, height: Boolean): Point {
        return Point(if (width) this.width / 2.0 else .0, if (height) this.height / 2.0 else .0)
    }

    val center: Point
        get() = center(width = true, height = true)

    val widthI: Int
        get() = width.roundToInt()

    val heightI: Int
        get() = height.roundToInt()

    override operator fun equals(other: Any?): Boolean {
        if (other !is Rectangle) {
            return false
        }
        return width == other.width && height == other.height || super.equals(other)
    }
}