package wtf.uuh.mc.pogui.mixins

import net.minecraft.client.Mouse
import net.minecraft.client.util.Window
import org.spongepowered.asm.mixin.Mixin
import org.spongepowered.asm.mixin.Shadow
import org.spongepowered.asm.mixin.injection.At
import org.spongepowered.asm.mixin.injection.Inject
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo
import wtf.uuh.mc.pogui.input.InputDispatcher
import wtf.uuh.mc.pogui.providers.MC

@Mixin(Mouse::class)
class MouseMixin {
    @Shadow
    private val x = 0.0

    @Shadow
    private val y = 0.0

    @Inject(at = [At(value = "TAIL")], method = ["onCursorPos(JDD)V"])
    fun onCursorPos(glfwWindow: Long, x: Double, y: Double, ci: CallbackInfo?) {
        val window: Window = MC.window
        val scaledX = x * window.scaledWidth / window.width.toDouble()
        val scaledY = y * window.scaledHeight / window.height.toDouble()
        InputDispatcher.onMousePosChange(scaledX, scaledY)
    }

    @Inject(at = [At(value = "TAIL")], method = ["onMouseButton(JIII)V"])
    fun onMouseButton(window: Long, button: Int, action: Int, mods: Int, ci: CallbackInfo?) {
        InputDispatcher.onMouseButtonStateChange(button, action, mods)
    }
}