package wtf.uuh.mc.pogui.mixins

import net.minecraft.client.gui.hud.InGameHud
import net.minecraft.client.util.math.MatrixStack
import org.spongepowered.asm.mixin.Mixin
import org.spongepowered.asm.mixin.injection.At
import org.spongepowered.asm.mixin.injection.Inject
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo
import wtf.uuh.mc.pogui.windowing.WindowManager

@Mixin(InGameHud::class)
class InGameHudMixin {
    @Inject(at = [At(value = "RETURN")], method = ["render"], cancellable = true)
    fun render(matrixStack: MatrixStack, delta: Float, ci: CallbackInfo?) {
        WindowManager.onRender(matrixStack)
    }
}