package wtf.uuh.mc.pogui.view

import wtf.uuh.mc.pogui.geometry.Point
import wtf.uuh.mc.pogui.input.MouseButton
import wtf.uuh.mc.pogui.layout.ViewSizing.FILL_PARENT
import wtf.uuh.mc.pogui.windowing.Window

class RootView(windowToAttachTo: Window) : ViewGroup() {

    init {
        window = windowToAttachTo
        parent = windowToAttachTo.viewGroupProxy

        layout {
            parameters {
                horizontalSizing = FILL_PARENT
                verticalSizing = FILL_PARENT
            }
        }
    }

    override infix fun addView(view: View) {
        if (childrenCount > 0) {
            throw RuntimeException("RootView can only contain one view")
        }
        super.addView(view)
        super.onAttachToWindow(window)
    }

    override fun removeView(view: View) {
        if (childrenCount == 0) {
            return
        }
        super.removeView(view)
        super.onDetachFromWindow()
    }

    fun removeView() {
        if (childrenCount == 1) {
            removeView(children[0])
        }
    }

    override fun onMouseClickStateChange(
        x: Double,
        y: Double,
        button: MouseButton,
        isClicked: Boolean
    ) {
        if (isClicked) {
            window!!.moveToFront()
        }
    }

    override val pos: Point
        get() = window!!.innerZeroPos ?: Point.zero

}