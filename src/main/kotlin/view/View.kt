package wtf.uuh.mc.pogui.view

import net.minecraft.util.math.Box
import org.apache.logging.log4j.core.Layout
import wtf.uuh.mc.pogui.geometry.Point
import wtf.uuh.mc.pogui.input.InputDispatcher
import wtf.uuh.mc.pogui.input.MouseButton
import wtf.uuh.mc.pogui.input.MouseEventListener
import wtf.uuh.mc.pogui.layout.LayoutParams
import wtf.uuh.mc.pogui.layout.ViewSizing
import wtf.uuh.mc.pogui.render.DrawContext
import wtf.uuh.mc.pogui.view.dsl.LayoutDefinition
import wtf.uuh.mc.pogui.windowing.Window
import java.util.*

abstract class View : MouseEventListener {

    var isVisible = true
        set(visible) {
            field = visible
            if (visible && isAttachedToWindow) {
                InputDispatcher.addMouseListener(this)
            } else if (!visible) {
                InputDispatcher.removeMouseListener(this)
            }
        }

    var parent: ViewGroup? = null
    private val clickListeners = ArrayList<OnClickListener>()
    private val secondaryClickListeners =
        ArrayList<OnSecondaryClickListener>()
    var layoutParams = LayoutParams()

    override val zIndex: Double
        get() = window?.zIndex ?: -1.0

    var window: Window? = null

    abstract fun draw(context: DrawContext)
    open val height: Double
        get() = when (layoutParams.verticalSizing) {
            ViewSizing.FILL_PARENT -> parent?.innerHeight ?: .0
            else -> .0
        }

    open val width: Double
        get() = when (layoutParams.horizontalSizing) {
            ViewSizing.FILL_PARENT -> parent?.innerWidth ?: .0
            else -> .0
        }

    open fun onAttachToWindow(window: Window?) {
        this.window = window
        if (isVisible) {
            InputDispatcher.addMouseListener(this)
        }
    }

    open fun onDetachFromWindow() {
        InputDispatcher.removeMouseListener(this)
        window = null
    }

    val isAttachedToWindow: Boolean
        get() = window != null

    open val pos: Point
        get() = parent!!.getViewPos(this)

    fun addOnClickListener(listener: OnClickListener) {
        clickListeners.add(listener)
    }

    fun removeOnClickListener(listener: OnClickListener) {
        clickListeners.remove(listener)
    }

    fun addOnSecondaryClickListener(listener: OnSecondaryClickListener) {
        secondaryClickListeners.add(listener)
    }

    fun removeOnSecondaryClickListener(listener: OnSecondaryClickListener) {
        secondaryClickListeners.remove(listener)
    }

    open val isEffectivelyVisible: Boolean
        get() = isVisible && parent?.isVisible ?: false

    override fun onMouseClick(x: Double, y: Double, button: MouseButton?) {
        when (button) {
            MouseButton.LEFT -> clickListeners.forEach { it() }
            MouseButton.RIGHT -> secondaryClickListeners.forEach { it() }
            else -> {
            }
        }
    }

    override val bounds: Box?
        get() {
            if (window == null) {
                return null
            }
            return Box(
                pos.x,
                pos.y,
                window!!.zIndex,
                pos.x + width,
                pos.y + height,
                window!!.zIndex
            )
        }

    override fun shouldListenToMouse(): Boolean {
        return isEffectivelyVisible
    }

}


typealias OnClickListener = () -> Unit
typealias OnSecondaryClickListener = () -> Unit