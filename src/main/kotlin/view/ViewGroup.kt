package wtf.uuh.mc.pogui.view

import net.minecraft.client.gui.screen.Screen
import wtf.uuh.mc.pogui.color.ColorComponents
import wtf.uuh.mc.pogui.geometry.Point
import wtf.uuh.mc.pogui.layout.LayoutParams
import wtf.uuh.mc.pogui.layout.ViewSizing
import wtf.uuh.mc.pogui.render.DrawContext
import wtf.uuh.mc.pogui.view.dsl.LayoutDefinition
import wtf.uuh.mc.pogui.windowing.Window
import java.util.*
import java.util.function.Consumer
import kotlin.math.ceil

/**
 * A ViewGroup without any layout logic
 * is a view that can contain subviews, each
 * being overlaid on top of each other.
 * The total width and height is the same as the
 * width and height of the largest view, respectively.
 */
abstract class ViewGroup : View() {
    private val views = ArrayList<View>()
    var backgroundColor = ColorComponents.transparent

    open infix fun addView(view: View) {
        if (!views.contains(view)) {
            view.parent = this
            views.add(view)
        }
    }

    open infix fun removeView(view: View) {
        views.remove(view)
        view.parent = null
    }

    operator fun invoke(childrenStructure: ViewGroup.() -> Unit) = childrenStructure()

    override fun onAttachToWindow(window: Window?) {
        super.onAttachToWindow(window)
        views.forEach(Consumer { view: View ->
            view.onAttachToWindow(
                window
            )
        })
    }

    override fun onDetachFromWindow() {
        super.onDetachFromWindow()
        views.forEach(Consumer { obj: View -> obj.onDetachFromWindow() })
    }

    override fun draw(context: DrawContext) {
        val viewPos = pos
        context.matrixStack.push()
        context.matrixStack.translate(viewPos.x, viewPos.y, 0.0)
        Screen.fill(
            context.matrixStack,
            0,
            0,
            ceil(width).toInt(),
            ceil(height).toInt(),
            backgroundColor.toColor()
        )
        context.matrixStack.pop()
        views.forEach(Consumer { view: View ->
            view.draw(
                context
            )
        })
    }

    override val height: Double
        get() {
            return if (layoutParams.verticalSizing == ViewSizing.AUTO) {
                (views
                    .filter { it.layoutParams.verticalSizing == ViewSizing.AUTO }
                    .map { it.height }
                    .max() ?: .0) + layoutParams.totalVerticalPadding
            } else super.height
        }

    override val width: Double
        get() {
            return if (layoutParams.horizontalSizing == ViewSizing.AUTO) {
                (views
                    .filter { it.layoutParams.horizontalSizing == ViewSizing.AUTO }
                    .map { it.width }
                    .max() ?: .0) + layoutParams.totalHorizontalPadding
            } else super.width
        }

    val children: List<View>
        get() = Collections.unmodifiableList(views)

    val childrenCount: Int
        get() = views.size

    val innerZeroPos: Point
        get() = pos + Point(layoutParams.leftPadding, layoutParams.topPadding)

    open fun getViewPos(view: View): Point {
        require(views.contains(view)) { "Specified view is not child of this view" }
        return innerZeroPos
    }

    open val innerHeight: Double
        get() = height - layoutParams.totalVerticalPadding

    open val innerWidth: Double
        get() = width - layoutParams.totalHorizontalPadding


    infix fun layout(layoutDef: LayoutDefinition.() -> Unit) {
        LayoutDefinition(this).apply(layoutDef)
    }

}