package wtf.uuh.mc.pogui.view.dsl

import wtf.uuh.mc.pogui.layout.LayoutParams
import wtf.uuh.mc.pogui.view.ViewGroup

class LayoutDefinition(val view: ViewGroup) {

    var layoutParams = LayoutParams()

    infix fun parameters(paramDef: LayoutParams.() -> Unit) {
        layoutParams.apply(paramDef)
    }

    infix fun views(viewStructure: ViewGroup.() -> Unit) {
        view.apply(viewStructure)
    }

    /*infix fun view() {

    }*/

}