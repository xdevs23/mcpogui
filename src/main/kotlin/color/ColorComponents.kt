package wtf.uuh.mc.pogui.color

data class ColorComponents(
    var red: Float = 0f,
    var green: Float = 0f,
    var blue: Float = 0f,
    var alpha: Float = 1f
) {
    companion object {
        val black get() = ColorComponents()
        val white get() = ColorComponents(1f, 1f, 1f)
        val transparent get() = ColorComponents(alpha = 0f)
    }

    fun toColor(): Int {
        return Coloring.toColor(this)
    }

    init {
        red = if (red <= 255f) red / 255f else red
        green = if (green <= 255f) green / 255f else green
        blue = if (blue <= 255f) blue / 255f else blue
    }
}