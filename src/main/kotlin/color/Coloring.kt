package wtf.uuh.mc.pogui.color

import kotlin.math.floor
import kotlin.math.min

object Coloring {
    fun extractColorComponents(color: Int): ColorComponents {
        return ColorComponents(
            (color shr 16 and 255).toFloat() / 255.0f,
            (color shr 8 and 255).toFloat() / 255.0f,
            (color and 255).toFloat() / 255.0f,
            (color shr 24 and 255).toFloat() / 255.0f
        )
    }

    fun toColor(color: ColorComponents): Int {
        val alpha = min(floor(color.alpha * 255.0), 255.0).toInt() shl 24
        val red = min(floor(color.red * 255.0), 255.0).toInt() shl 16
        val green = min(floor(color.green * 255.0), 255.0).toInt() shl 8
        val blue = min(floor(color.blue * 255.0), 255.0).toInt()
        return alpha or red or green or blue
    }
}