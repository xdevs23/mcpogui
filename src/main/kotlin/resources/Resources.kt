package wtf.uuh.mc.pogui.resources

import com.google.gson.Gson
import java.io.InputStream
import kotlin.reflect.KClass

inline class ResourcePath(val path: String)

inline infix fun <R> ResourcePath.open(whileOpened: (InputStream) -> R): R =
    javaClass.getResourceAsStream(this.path).use(whileOpened)

infix fun <T : Any> ResourcePath.loadJSONAs(cls: KClass<T>): T =
    open {
        Gson().fromJson(it.bufferedReader(), cls.java)
    }

