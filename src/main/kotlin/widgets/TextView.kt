package wtf.uuh.mc.pogui.widgets

import wtf.uuh.mc.pogui.color.ColorComponents
import wtf.uuh.mc.pogui.render.DrawContext
import wtf.uuh.mc.pogui.render.text.TextRenderer
import wtf.uuh.mc.pogui.view.View

class TextView(
    var text: String = "",
    var color: ColorComponents = ColorComponents.white
) : View() {

    override fun draw(context: DrawContext) {
        TextRenderer.draw(context.matrixStack, text, pos, color)
    }

    override val height: Double
        get() = TextRenderer.textHeight()

    override val width: Double
        get() = TextRenderer.textWidth(text).toDouble()

}