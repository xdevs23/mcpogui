package wtf.uuh.mc.pogui.render

import net.minecraft.client.gui.screen.Screen
import net.minecraft.client.util.math.MatrixStack
import wtf.uuh.mc.pogui.color.ColorComponents
import wtf.uuh.mc.pogui.geometry.Rectangle

object RenderImpl {
    fun drawRectangle(
        matrixStack: MatrixStack?,
        rectangle: Rectangle,
        color: ColorComponents?
    ) {
        Screen.fill(
            matrixStack,
            0,
            0,
            rectangle.widthI,
            rectangle.heightI,
            color!!.toColor()
        )
    }
}