package wtf.uuh.mc.pogui.render.text

import net.minecraft.client.util.math.MatrixStack
import wtf.uuh.mc.pogui.color.ColorComponents
import wtf.uuh.mc.pogui.geometry.Point
import wtf.uuh.mc.pogui.providers.MC

object TextRenderer {
    fun draw(
        matrixStack: MatrixStack?,
        text: String?,
        pos: Point?,
        color: ColorComponents?
    ) {
        MC.textRenderer.draw(matrixStack, text, pos!!.x.toFloat(), pos.y.toFloat(), color!!.toColor())
    }

    fun draw(matrixStack: MatrixStack?, text: String?, color: ColorComponents?) {
        draw(
            matrixStack,
            text,
            Point.zero,
            color
        )
    }

    fun textHeight(): Double {
        return MC.textRenderer.fontHeight.toDouble()
    }

    fun textWidth(text: String?): Int {
        return MC.textRenderer.getWidth(text)
    }
}