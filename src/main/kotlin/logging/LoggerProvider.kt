package wtf.uuh.mc.pogui.logging

import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import org.apache.logging.log4j.util.ReflectionUtil
import wtf.uuh.mc.pogui.modInfo

private val loggerCache = hashMapOf<String, Logger>()

val logger: Logger
    get() {
        val name = modInfo.id + "|" + ReflectionUtil.getCallerClass(2).simpleName
        return loggerCache.getOrElse(name, {
            val logger = LogManager.getFormatterLogger(name)
            loggerCache[name] = logger
            logger
        })
    }