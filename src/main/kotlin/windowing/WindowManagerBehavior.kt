package wtf.uuh.mc.pogui.windowing

import wtf.uuh.mc.pogui.geometry.Point

abstract class WindowManagerBehavior(val windowManager: WindowManager) {
    abstract fun windowPlacementOrientation(): WindowPlacement
    abstract fun nextWindowPosition(window: Window): Point
    abstract fun resetWindowPosition()
}