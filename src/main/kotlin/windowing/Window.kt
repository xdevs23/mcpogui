package wtf.uuh.mc.pogui.windowing

import net.minecraft.client.util.math.MatrixStack
import wtf.uuh.mc.pogui.geometry.Point
import wtf.uuh.mc.pogui.layout.LinearLayout
import wtf.uuh.mc.pogui.layout.ViewSizing.FILL_PARENT
import wtf.uuh.mc.pogui.render.DrawContext
import wtf.uuh.mc.pogui.view.RootView
import wtf.uuh.mc.pogui.view.View
import wtf.uuh.mc.pogui.view.ViewGroup
import wtf.uuh.mc.pogui.windowing.decorations.WindowDecoration
import wtf.uuh.mc.pogui.windowing.theming.DefaultWindowDecoration

class Window(val windowManager: WindowManager) {
    companion object {
        const val INITIAL_WIDTH = 120
        const val INITIAL_HEIGHT = 80
    }

    var isVisible = true
        private set
    val isPinned = false

    val viewGroupProxy = object : ViewGroup() {
        override val height: Double get() = this@Window.height
        override val width: Double get() = this@Window.width
        override val innerHeight: Double get() = this@Window.innerHeight
        override val innerWidth: Double get() = this@Window.innerWidth
        override val isEffectivelyVisible: Boolean get() = this@Window.isEffectivelyVisible
    }

    /**
     * Overwrites existing window manager positioning
     * and is mainly used for allowing the user to move the windows
     */
    private var overwrittenPos: Point? = null
    private var position: Point = Point.zero
    val decoration: WindowDecoration = DefaultWindowDecoration()

    var title: String = ""
        set(text) {
            field = text
            windowTitleBar.onTitleChange()
        }

    private val rootView = RootView(this)
    private val contentView: LinearLayout = LinearLayout()
    private val windowTitleBar = WindowTitleBar()
    var zIndex = 0.0

    init {
        contentView.layout {
            parameters {
                horizontalSizing = FILL_PARENT
                verticalSizing = FILL_PARENT
            }
            views {
                windowTitleBar
            }
        }

        onDecorationChange()
        setContentView(contentView)
    }

    private fun onDecorationChange() {
        windowTitleBar.isVisible = decoration.shouldShowTitleBar
        if (windowTitleBar.isVisible) {
            windowTitleBar.onDecorationChange()
        }
    }

    /**
     * Makes the window disappear.
     * The window manager will forget about it.
     */
    fun close() {
        windowManager.close(this)
    }

    /**
     * Makes the window appear,
     * the window manager will manage it
     */
    fun open() {
        windowManager.register(this)
    }

    fun render(matrixStack: MatrixStack) {
        applyOverwrittenPosition()
        WindowRenderer.render(
            WindowRenderContext(
                this, matrixStack
            )
        )
        if (canDrawContent) {
            rootView.draw(DrawContext(matrixStack))
        }
    }

    val decorationWidth get() = decoration.windowBorderThickness + decoration.windowPadding
    val totalDecorationWidth get() = decorationWidth * 2
    val decorationHeight get() = decoration.windowBorderThickness + decoration.windowPadding
    val totalDecorationHeight get() = decorationHeight * 2

    val innerWidth get() = width - totalDecorationWidth
    val innerHeight get() = height - totalDecorationHeight

    val width get() = INITIAL_WIDTH + totalDecorationWidth
    val height get() = INITIAL_HEIGHT + totalDecorationHeight

    val endX get() = position.x + width
    val endY get() = position.y + height

    var pos: Point
        get() = position.copy()
        set(pos) {
            position = pos.copy()
        }

    val innerZeroPos: Point?
        get() = pos + Point(x = decoration.windowBorderThickness + decoration.windowPadding)

    val isNotPinned: Boolean
        get() = !isPinned

    fun overwritePos(pos: Point) {
        overwrittenPos = pos
    }

    val isPositionOverwritten get() = overwrittenPos != null

    fun applyOverwrittenPosition() {
        if (isPositionOverwritten) {
            pos = overwrittenPos!!
        }
    }

    /**
     * Makes the window visible
     * If you want to open the window, use open()
     */
    fun show() {
        if (!isVisible) {
            isVisible = true
        }
    }

    /**
     * Makes the window invisible
     * If you want to close the window, use close()
     */
    fun hide() {
        if (isVisible) {
            isVisible = false
        }
    }

    private fun setContentView(view: View) {
        rootView.removeView()
        rootView addView view
    }

    private val canDrawContent get() = isVisible && contentView.isAttachedToWindow && contentView.isVisible

    val isEffectivelyVisible
        get() = isVisible && (windowManager.isVisible || isPinned)

    fun moveToFront() = windowManager.moveToFront(this)
    fun moveToBack() = windowManager.moveToBack(this)

}