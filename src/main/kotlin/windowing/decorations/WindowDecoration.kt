package wtf.uuh.mc.pogui.windowing.decorations

import wtf.uuh.mc.pogui.color.ColorComponents
import wtf.uuh.mc.pogui.layout.LayoutParams

abstract class WindowDecoration {

    abstract val windowPadding: Double
    abstract val windowBorderThickness: Double
    abstract val titleBarParams: LayoutParams

    abstract val shouldShowTitleBar: Boolean
    abstract val backgroundColor: ColorComponents
    abstract val titleBarBackgroundColor: ColorComponents
    abstract val textColor: ColorComponents
    abstract val titleBarTextColor: ColorComponents

}
