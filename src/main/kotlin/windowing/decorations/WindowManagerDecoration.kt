package wtf.uuh.mc.pogui.windowing.decorations

import wtf.uuh.mc.pogui.color.ColorComponents

abstract class WindowManagerDecoration {
    abstract val backgroundColor: ColorComponents
}