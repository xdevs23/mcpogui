package wtf.uuh.mc.pogui.windowing

import wtf.uuh.mc.pogui.geometry.Point
import wtf.uuh.mc.pogui.input.MouseButton
import wtf.uuh.mc.pogui.layout.ViewSizing.FILL_PARENT
import wtf.uuh.mc.pogui.view.ViewGroup
import wtf.uuh.mc.pogui.widgets.TextView

class WindowTitleBar : ViewGroup() {

    private val title = TextView()
    private var isDraggingWindow = false
    private var dragDelta = Point.zero

    fun onDecorationChange() {
        val decor = window!!.decoration
        backgroundColor = decor.titleBarBackgroundColor
        title.color = decor.titleBarTextColor
    }

    fun onTitleChange() {
        title.text = window!!.title
    }

    override fun onMouseClickStateChange(
        x: Double,
        y: Double,
        button: MouseButton,
        isClicked: Boolean
    ) {
        if (button == MouseButton.LEFT) {
            if (isDraggingWindow && isClicked) {
                return
            }
            isDraggingWindow = isClicked
            if (isClicked) {
                val mousePos = Point(x, y)
                dragDelta = mousePos - pos
            }
        }
    }

    override fun onMouseMove(x: Double, y: Double) {
        if (isDraggingWindow) {
            val mousePos = Point(x, y)
            val delta = mousePos - pos
            val moveDelta = delta - dragDelta
            val wm = window!!.windowManager
            window!!.overwritePos(
                (window!!.pos + moveDelta)
                    .clamp(
                        0.0, 0.0,
                        wm.width - window!!.width, wm.height - window!!.height
                    )
            )
        }
    }

    init {
        layout {
            parameters {
                padding(4.0)
                horizontalSizing = FILL_PARENT
            }
            views {
                title
            }
        }
    }
}