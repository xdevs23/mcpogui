package wtf.uuh.mc.pogui.windowing

enum class WindowPlacement {
    /**
     * When a window first appears, it is positioned
     * vertically after the next window (aka column mode)
     */
    VERTICAL_BY_DEFAULT
}