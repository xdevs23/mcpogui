package wtf.uuh.mc.pogui.windowing

interface WindowListener {
    fun onClose(window: Window)
    fun onOpen(window: Window)
}