package wtf.uuh.mc.pogui.windowing.theming

import wtf.uuh.mc.pogui.color.ColorComponents
import wtf.uuh.mc.pogui.layout.LayoutParams
import wtf.uuh.mc.pogui.windowing.decorations.WindowDecoration

class DefaultWindowDecoration : WindowDecoration() {
    override val windowBorderThickness = .0
    override val titleBarParams = LayoutParams(4.0, 2.0)
    override val windowPadding = 2.0
    override val backgroundColor = ColorComponents(alpha = .72f)
    override val titleBarBackgroundColor = ColorComponents(.06f, .06f, .06f)
    override val textColor = ColorComponents.white
    override val titleBarTextColor = ColorComponents.white
    override val shouldShowTitleBar = true
}