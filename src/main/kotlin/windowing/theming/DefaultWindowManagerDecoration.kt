package wtf.uuh.mc.pogui.windowing.theming

import wtf.uuh.mc.pogui.color.ColorComponents
import wtf.uuh.mc.pogui.windowing.decorations.WindowManagerDecoration

class DefaultWindowManagerDecoration : WindowManagerDecoration() {
    override val backgroundColor: ColorComponents
        get() = ColorComponents.transparent
}