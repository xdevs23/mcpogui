package wtf.uuh.mc.pogui.windowing

import wtf.uuh.mc.pogui.geometry.Rectangle
import wtf.uuh.mc.pogui.render.RenderImpl

/**
 * Runs things after pushing and manipulating a matrixStack
 */
typealias ContextRunner = () -> Unit

object WindowRenderer {
    fun runTranslated(
        ctx: WindowRenderContext,
        x: Double,
        y: Double,
        runner: ContextRunner
    ) {
        ctx.matrixStack.push()
        ctx.matrixStack.translate(x, y, 0.0)
        runner()
        ctx.matrixStack.pop()
    }

    fun atWindow(ctx: WindowRenderContext, runner: ContextRunner) {
        runTranslated(ctx, ctx.window.pos.x, ctx.window.pos.y, runner)
    }

    fun render(ctx: WindowRenderContext) {
        val win = ctx.window
        atWindow(ctx) {
            RenderImpl.drawRectangle(
                ctx.matrixStack,
                Rectangle(win.width, win.height),
                win.decoration.backgroundColor
            )
        }
    }

}