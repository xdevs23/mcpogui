package wtf.uuh.mc.pogui.windowing

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import net.minecraft.client.gui.DrawableHelper
import net.minecraft.client.util.math.MatrixStack
import wtf.uuh.mc.pogui.providers.MC
import wtf.uuh.mc.pogui.windowing.decorations.WindowManagerDecoration
import wtf.uuh.mc.pogui.windowing.theming.DefaultWindowManagerDecoration
import java.util.*
import java.util.function.Consumer

class WindowManager {
    private val windows = arrayListOf<Window>()
    private val windowListeners = arrayListOf<WindowListener>()
    private val decoration: WindowManagerDecoration = DefaultWindowManagerDecoration()
    private val behavior: WindowManagerBehavior = VerticalTilingBehavior(this)

    var isVisible = true
        get() = field && !isScreenClosed

    private var isScreenClosed = true
    private var wasClosedLastFrame = false

    val width get() = MC.window.scaledWidth
    val height get() = MC.window.scaledHeight

    private val visibleWindows
        get() = windows
            .filter { it.isVisible }
            .sortedByDescending { it.zIndex }

    init {
        allInstances.add(this)
    }

    fun destroy() {
        allInstances.remove(this)
    }

    fun close(window: Window?) {
        windows.remove(window)
    }

    private fun notifyOnWindowClose(window: Window) {
        windowListeners.forEach(Consumer { listener: WindowListener ->
            listener.onClose(
                window
            )
        })
    }

    fun register(window: Window) {
        windows.add(window)
        window.zIndex = windows.size.toDouble()
    }

    private fun notifyOnWindowOpen(window: Window) {
        windowListeners.forEach(Consumer { listener: WindowListener ->
            listener.onOpen(
                window
            )
        })
    }

    fun addWindowListener(listener: WindowListener) {
        windowListeners.add(listener)
    }

    fun removeWindowListener(listener: WindowListener?) {
        windowListeners.remove(listener)
    }

    fun render(matrixStack: MatrixStack) {
        behavior.resetWindowPosition()
        windows.forEach(Consumer { window: Window ->
            window.pos = behavior.nextWindowPosition(window)
        })

        if (isVisible) {
            this.draw(matrixStack)
            visibleWindows.forEach { window: Window ->
                window.render(
                    matrixStack
                )
            }
        } else if (!wasClosedLastFrame) {
            visibleWindows.filter { obj: Window -> obj.isPinned }
                .forEach { window: Window ->
                    window.render(
                        matrixStack
                    )
                }
        }
    }

    private fun draw(matrixStack: MatrixStack?) {
        if (isVisible) {
            DrawableHelper.fill(matrixStack, 0, 0, width, height, decoration.backgroundColor.toColor())
        }
    }

    val isInvisible: Boolean
        get() = !isVisible

    fun hide() {
        isVisible = false
    }

    fun show() {
        isVisible = true
    }

    fun moveToFront(window: Window) {
        val currentZ = window.zIndex
        val newHighestIndex = windows.size.toDouble()
        window.zIndex = newHighestIndex
        windows
            .filter { it.zIndex >= currentZ }
            .forEach { moveBackward(it) }
    }

    fun moveToBack(window: Window) {
        windows.forEach { moveForward(it) }
        window.zIndex = 0.0
    }

    fun moveBackward(window: Window) {
        window.zIndex = window.zIndex - 1
    }

    fun moveForward(window: Window) {
        window.zIndex = window.zIndex + 1
    }

    companion object {
        private val allInstances = arrayListOf<WindowManager>()

        fun onRender(matrixStack: MatrixStack) {
            allInstances.forEach {
                GlobalScope.launch {
                    it.render(matrixStack)
                }
            }
        }
    }

}