package wtf.uuh.mc.pogui.windowing

import net.minecraft.client.util.math.MatrixStack

data class WindowRenderContext(
    val window: Window,
    val matrixStack: MatrixStack
) 