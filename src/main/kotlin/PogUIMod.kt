package wtf.uuh.mc.pogui

import net.fabricmc.api.ModInitializer
import wtf.uuh.mc.pogui.logging.logger
import wtf.uuh.mc.pogui.resources.ResourcePath
import wtf.uuh.mc.pogui.resources.loadJSONAs

data class ModInfo(
    val id: String,
    val name: String,
    val version: String
)

lateinit var modInfo: ModInfo

private fun loadModInfo() {
    modInfo = ResourcePath("/fabric.mod.json") loadJSONAs ModInfo::class
}

/**
 * This serves the mere purpose of testing whether
 * it compiles and starts – this is not supposed to
 * be used as a mod – rather as a library.
 */
class PogUIMod : ModInitializer {

    override fun onInitialize() {
        loadModInfo()
        logger.info("Initializing ${modInfo.name} (${modInfo.id}) Version ${modInfo.version}")
    }
}
