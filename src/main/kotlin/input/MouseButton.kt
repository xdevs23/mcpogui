package wtf.uuh.mc.pogui.input

import org.lwjgl.glfw.GLFW

enum class MouseButton {
    /**
     * Usually the left mouse button
     */
    LEFT,

    /**
     * Usually the right mouse button
     */
    RIGHT,

    /**
     * Scroll wheel / middle mouse button
     */
    MIDDLE;

    companion object {
        fun fromGlfwButton(button: Int): MouseButton {
            return when (button) {
                GLFW.GLFW_MOUSE_BUTTON_RIGHT -> RIGHT
                GLFW.GLFW_MOUSE_BUTTON_MIDDLE -> MIDDLE
                GLFW.GLFW_MOUSE_BUTTON_LEFT -> LEFT
                else -> LEFT
            }
        }
    }
}