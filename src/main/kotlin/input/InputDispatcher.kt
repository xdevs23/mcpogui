package wtf.uuh.mc.pogui.input

import net.minecraft.util.math.Box
import org.lwjgl.glfw.GLFW
import wtf.uuh.mc.pogui.geometry.Point
import java.util.*
import java.util.function.Consumer

object InputDispatcher {

    private var mousePosition: Point = Point.zero
    private val buttonClickStates = HashMap<MouseButton, Boolean>()
    private val mouseDownActiveStates =
        HashMap<MouseEventListener, Boolean>()
    private val mouseListeners =
        ArrayList<MouseEventListener>()

    fun onMousePosChange(mouseX: Double, mouseY: Double) {
        val lastPos = mousePosition
        mousePosition = Point(mouseX, mouseY)
        if (lastPos != mousePosition) {
            dispatchMouseMoveEvent(lastPos)
        }
    }

    fun onMouseButtonStateChange(button: Int, action: Int, mods: Int) {
        val mouseButton: MouseButton = MouseButton.fromGlfwButton(button)
        val wasClicked = buttonClickStates[mouseButton]
        val isClicked = action == GLFW.GLFW_PRESS
        buttonClickStates[mouseButton] = isClicked
        if (wasClicked != null && wasClicked != isClicked) {
            dispatchMouseClickEvent(mouseButton, isClicked)
        }
    }

    private fun dispatchMouseMoveEvent(lastMousePos: Point) {
        val allTopMostWithin2DBounds = allTopMostWithin2DBounds()
        mouseListeners.forEach { listener ->
            if (!listener.shouldListenToMouse()) {
                return@forEach
            }
            val bounds = listener.bounds
            val previouslyWithinBounds = isWithinBounds(bounds, lastMousePos)
            if (allTopMostWithin2DBounds.contains(listener)) {
                // mouse is withing bounds, so we can notify the listener
                listener.onMouseMove(mousePosition.x, mousePosition.y)
                if (!previouslyWithinBounds) {
                    // mouse has entered the target bounds
                    listener.onMouseEnter()
                }
            } else {
                if (previouslyWithinBounds) {
                    // moue has left the target bounds
                    listener.onMouseLeave()
                }
            }
        }
    }

    private fun isWithinBounds(bounds: Box?, mousePosition: Point): Boolean {
        return mousePosition.x >= bounds!!.minX && mousePosition.y >= bounds.minY &&
                mousePosition.x <= bounds.maxX && mousePosition.y <= bounds.maxY
    }

    private fun isZIndexWithinBounds(bounds: Box?, zIndex: Double): Boolean {
        return zIndex >= bounds!!.minZ && zIndex <= bounds.maxZ
    }

    private fun dispatchMouseClickEvent(mouseButton: MouseButton, isClicked: Boolean) {
        allTopMostWithin2DBounds().forEach(Consumer { listener: MouseEventListener ->
            if (isClicked) {
                mouseDownActiveStates[listener] = true
            }
            listener.onMouseClickStateChange(
                mousePosition.x, mousePosition.y, mouseButton, isClicked
            )
            if (!isClicked) {
                listener.onMouseClick(mousePosition.x, mousePosition.y, mouseButton)
                mouseDownActiveStates.clear()
            }
        })
    }

    private fun allTopMostWithin2DBounds(): List<MouseEventListener> {
        val topMost = topMostWithin2DBounds() ?: return listOf()
        return allWithin2DBounds().filter { listener: MouseEventListener ->
            isZIndexWithinBounds(
                listener.bounds,
                topMost.zIndex
            )
        }
    }

    private fun validListeners(): List<MouseEventListener> {
        return mouseListeners
            .filter(MouseEventListener::shouldListenToMouse)
            .filter { it.bounds != null }
    }

    private fun allWithin2DBounds(): List<MouseEventListener> {
        return validListeners()
            .filter { listener ->
                isWithinBounds(listener.bounds, mousePosition) ||
                        mouseDownActiveStates.getOrDefault(listener, false)
            }
    }

    private fun topMostWithin2DBounds(): MouseEventListener? {
        return allWithin2DBounds().maxBy { it.zIndex }
    }

    fun addMouseListener(listener: MouseEventListener) {
        mouseListeners.add(listener)
    }

    fun removeMouseListener(listener: MouseEventListener?) {
        mouseListeners.remove(listener)
    }
}