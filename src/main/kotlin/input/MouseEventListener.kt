package wtf.uuh.mc.pogui.input

import net.minecraft.util.math.Box

interface MouseEventListener {
    fun onMouseMove(x: Double, y: Double) {}
    fun onMouseClickStateChange(
        x: Double,
        y: Double,
        button: MouseButton,
        isClicked: Boolean
    ) {
    }

    fun onMouseClick(x: Double, y: Double, button: MouseButton?) {}
    fun onMouseEnter() {}
    fun onMouseLeave() {}
    fun shouldListenToMouse(): Boolean {
        return true
    }

    /**
     * Bounds inside of which events are triggered for.
     * @return bounds. null means no events.
     */
    val bounds: Box?
        get() = null

    val zIndex: Double
        get() = .0
}