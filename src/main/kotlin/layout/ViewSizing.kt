package wtf.uuh.mc.pogui.layout

enum class ViewSizing {
    AUTO, FILL_PARENT
}