package wtf.uuh.mc.pogui.layout

class LayoutParams(
    var topPadding: Double = 0.0,
    var rightPadding: Double = 0.0,
    var bottomPadding: Double = 0.0,
    var leftPadding: Double = 0.0
) {

    var horizontalSizing = ViewSizing.AUTO
    var verticalSizing = ViewSizing.AUTO

    constructor(padding: Double) : this() {
        padding(padding)
    }

    var totalVerticalPadding: Double
        get() = topPadding + bottomPadding
        set(value) {
            topPadding = value / 2.0
            bottomPadding = value / 2.0
        }

    var totalHorizontalPadding: Double
        get() = leftPadding + rightPadding
        set(value) {
            leftPadding = value / 2.0
            rightPadding = value / 2.0
        }

    /**
     * Behaves like CSS
     */
    fun padding(top: Double, right: Double? = null, bottom: Double? = null, left: Double? = null) {
        topPadding = top
        rightPadding = right ?: topPadding
        bottomPadding  = bottom ?: topPadding
        leftPadding = left ?: rightPadding
    }


}