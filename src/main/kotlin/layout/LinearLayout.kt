package wtf.uuh.mc.pogui.layout

import wtf.uuh.mc.pogui.geometry.Point
import wtf.uuh.mc.pogui.view.View
import wtf.uuh.mc.pogui.view.ViewGroup

class LinearLayout(private val orientation: Orientation = Orientation.VERTICAL) : ViewGroup() {
    override fun getViewPos(view: View): Point {
        val viewPos = super.getViewPos(view)
        for (child in children) {
            if (child === view) {
                break
            }
            viewPos += when (orientation) {
                Orientation.VERTICAL -> Point(y = child.height)
                Orientation.HORIZONTAL -> Point(x = child.width)
            }
        }
        return viewPos
    }

    enum class Orientation {
        VERTICAL, HORIZONTAL
    }

}