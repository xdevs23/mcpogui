#!/bin/bash

file_to_install=$(find build/libs -type f -name '*.jar' | grep -E 'pogui-[0-9]+?[.][0-9]+?[.][0-9]+?[.]jar$' | tail -n1)

echo "About to install $file_to_install"

mvn install:install-file \
	-DgroupId=wtf.uuh.mc \
	-DartifactId=pogui \
	-Dversion=0.0.0-dev \
	-Dfile=$file_to_install \
	-Dpackaging=jar \
	-DgeneratePom=true

